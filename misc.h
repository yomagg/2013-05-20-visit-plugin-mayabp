/**
 * misc.h
 *
 *  Created on: Apr 5, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */

#ifndef MISC_H_
#define MISC_H_

typedef enum diag_ {

	DIAG_ERR_NOT_EMPTY = -7,
	DIAG_ERR_NOT_A_SCALAR = -6,

	DIAG_ERR_MEM_ALLOCATION = -5,

	DIAG_ERR_NOT_SUPPORTED = -4,

	DIAG_ERR_ADIOS = -3,    //! ADIOS-related error
	DIAG_ERR_NULL_PTR = -2,

	DIAG_ERR = -1,//!< DIAG_ERR
	DIAG_OK = 0   //!< DIAG_OK
} diag_t;


#endif /* MISC_H_ */
