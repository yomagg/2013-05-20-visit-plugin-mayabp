/**
 * @file: debug.h
 * @author: Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot com
 * @date: Dec 19, 2012
 * Modified: Jan 03, 2013
 * The debugging utility macros
 */


#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdio.h>
#include <stdlib.h>

//! indicates that the debugging is on; comment if you don't want to have
//! debug
#define DBG_ON 1

//! indicates if you should run tests; comment if you don't want to run
//! test cases
#define TESTS_ON 1

//! Debug printing verbosity
#define DBG_LEVEL   DBG_DEBUG

// New debug messaging state. There is no sense of a "level" for debugging. Each of these define the
// purpose of the messages and is enabled/disabled per file

//! system cannot continue, e.g. malloc
#define DBG_ERROR   0
//!
#define DBG_CRITICAL 1
//! some serious problems
#define DBG_WARNING 2
#define DBG_MESSAGE 3
//! messages about state or configuration; high-level flow
#define DBG_INFO    4
//!  func args, variable values, etc; full flow, may slow system down
#define DBG_DEBUG   5

//! indicator that something should be done here
#define DBG_TODO	-1

//! test warn
#define TEST_WARN -2
//! test failed
#define TEST_FAILED -1
//! test passed
#define TEST_PASSED 1


#define DBG_ERROR_STR 		"ERROR\t"
#define DBG_CRITICAL_STR 	"CRITICAL\t"
#define DBG_WARNING_STR 	"WARNING\t"
#define DBG_MESSAGE_STR 	"MESSAGE\t"
#define DBG_INFO_STR		"INFO\t"
#define DBG_DEBUG_STR		"DEBUG\t"
#define DBG_TODO_STR		"TODO\t"
#define DBG_NOTE_STR		"NOTE\t"
#define DBG_TEST_FAILED_STR		"TEST_FAILED\t"
#define DBG_TEST_PASSED_STR		"TEST_PASSED\t"
#define DBG_TEST_WARN_STR 	"TEST_WARN\t"


#define p_test_failed(fmt, args...)                             \
    do {                                                  \
         printf("%s(%d) %s:%s:%d: ", DBG_TEST_FAILED_STR, (TEST_FAILED), __FILE__, __FUNCTION__, __LINE__);  \
         printf(fmt, ##args);                                       \
         fflush(stdout);  											\
    } while(0)

#define p_test_passed(fmt, args...)                             \
    do {                                                  \
         printf("%s(%d) %s:%s:%d: ", DBG_TEST_PASSED_STR, (TEST_PASSED), __FILE__, __FUNCTION__, __LINE__);  \
         printf(fmt, ##args);                                       \
         fflush(stdout);  											\
    } while(0)

#define p_test_warn(fmt, args...)                             \
    do {                                                  \
         printf("%s(%d) %s:%s:%d: ", DBG_TEST_WARN_STR, (TEST_WARN), __FILE__, __FUNCTION__, __LINE__);  \
         printf(fmt, ##args);                                       \
         fflush(stdout);  											\
    } while(0)


#define p_note(fmt, args...)                             \
    do {                                                  \
         printf("%s(%d) %s:%s:%d: ", DBG_NOTE_STR, (DBG_TODO), __FILE__, __FUNCTION__, __LINE__);  \
         printf(fmt, ##args);                                       \
         fflush(stdout);  											\
    } while(0)


#define p_todo(fmt, args...)                             \
    do {                                                  \
         printf("%s(%d) %s:%s:%d: ", DBG_TODO_STR, (DBG_TODO), __FILE__, __FUNCTION__, __LINE__);  \
         printf(fmt, ##args);                                       \
         fflush(stdout);  											\
    } while(0)

#define p_not_impl()	\
	do {                                                  \
         printf("%s(%d) %s:%s:%d: Not implemented!\n", DBG_TODO_STR, (DBG_TODO), __FILE__, __FUNCTION__, __LINE__);  \
         fflush(stdout);  											\
    } while(0)



#define p_exit(fmt, args...)                             \
    do {                                                  \
         printf("%s(%d) %s:%s:%d: ", DBG_ERROR_STR, (DBG_ERROR), __FILE__, __FUNCTION__, __LINE__);  \
         printf(fmt, ##args);                                       \
         fflush(stdout);  											\
         exit(-1);													\
    } while(0)


//! @todo do something like that but smarter without unnecessary copying
#define p_error(fmt, args...)                             				\
    do {                                                                \
        if((DBG_ERROR) <= DBG_LEVEL) {                                  \
            printf("%s(%d) %s:%s:%d: ", DBG_ERROR_STR, (DBG_ERROR), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);  											\
        }                                                               \
    } while(0)


#define p_critical(fmt, args...) \
	do {                                                                \
        if((DBG_CRITICAL) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_CRITICAL_STR, (DBG_CRITICAL), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_warn(fmt, args...) \
	do {                                                                \
        if((DBG_WARNING) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_WARNING_STR, (DBG_WARNING), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_mesg(fmt, args...) \
	do {                                                                \
        if((DBG_MESSAGE) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_MESSAGE_STR, (DBG_MESSAGE), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_info(fmt, args...) \
	do {                                                                \
        if((DBG_INFO) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_INFO_STR, (DBG_INFO), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_debug(fmt, args...) \
	do {                                                                \
        if((DBG_DEBUG) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_DEBUG_STR, (DBG_DEBUG), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)


// ------------------------------------
// other debugging util macro
/**
 * exits if the pointer is null
 * @param ptr the pointer to be checked
 * @param mesg The message that might be showed
 */
#define ptr_null_exit(ptr, mesg) 	\
	if( NULL == ptr )				\
	  p_exit( "The pointer is NULL. Quitting..., %s\n", mesg);

/**
 * warns if the pointer is null
 * @param ptr the pointer to be checked
 * @param mesg The message to be displayed to the user
 */
#define ptr_null_warn(ptr, mesg)	\
	if( NULL == ptr )				\
		p_warn( "The pointer is NULL. %s\n", mesg);

// ----------------------------------------------------------------
// non-debug utility macros
// ----------------------------------------------------------------

/**
 * This time is used for defining the errmsg for throw_exception1.
 */
typedef char errmsg_t [1024];

/**
 * The macro expects that errmsg is defined as an array of characters
 * char errnmsg[1024] big enough to store the mesg
 * @param The name of the exception for VisIt e.g., InvalidVariableException
 * @param format The format of the string
 * @param args The arguments for the format
 */
#define throw_exception1(exception, format, args...) 							\
	do { 												\
		sprintf(errmsg, format, ##args); 					\
		p_error("%s", errmsg); 							\
		EXCEPTION1(exception, errmsg); 	\
	} while(0)



#endif /* DEBUG_H_ */
