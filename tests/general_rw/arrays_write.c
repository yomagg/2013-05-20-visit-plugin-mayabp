#include <stdio.h>
#include "mpi.h"
#include "adios.h"
int main (int argc, char ** argv) 
{
    char        filename [256];
    int         rank;
    int			X = 2;
    int 		Y = 3;
    int         NX = 10;
    double      t[NX];		// 1D variable
    double 		t_2d[X][Y]; // 2D variable
    double		t_3d[X][Y][Y]; // 3D variable
    int         i,j, k;
    char        result[1024], s[32];

    /* ADIOS variables declarations for matching gwrite_temperature.ch */
    int         adios_err;
    uint64_t    adios_groupsize, adios_totalsize;
    int64_t     adios_handle;
    MPI_Comm    comm =  MPI_COMM_WORLD;

    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);

    for (i=0; i<NX; i++){
        t[i] = rank*NX + i;
    }

    for (i=0; i < X; i++){
    	for( j= 0; j < Y; j++){
    		t_2d[i][j] = rank * 10 + j +i;
    	}
    }

    for (i = 0; i < X; i++){
    	for( j=0; j <Y; j++){
    		for( k= 0; k < Y; k++){
    			t_3d[i][j][k] = rank * 1000 + i*100 + j*10 + k;
    		}
    	}
    }

    sprintf (filename, "restart.bp");
    adios_init ("config.xml");
    adios_open (&adios_handle, "temperature", filename, "w", &comm);
    // the NX size, and NX size of elements for t[]
    // size of X and size of Y
    adios_groupsize = sizeof(int) + sizeof(double) * (NX) +
    		sizeof(int) + sizeof(int) + sizeof(double) * (X) * (Y) +
    		sizeof(double) * (X) * (Y) *(Y);
    adios_group_size (adios_handle, adios_groupsize, &adios_totalsize);

    // write the 1D variable, including its sizes
    adios_write (adios_handle, "NX", &NX);
    adios_write (adios_handle, "temperature", t);

    // write the 2D variable including its sizes
    adios_write (adios_handle, "X", &X);
    adios_write (adios_handle, "Y", &Y);
    adios_write (adios_handle, "t_2Dim", t_2d);

    // write the 3D variable, sizes are recorded
    adios_write (adios_handle, "t_3Dim", t_3d);

    adios_close (adios_handle);
    adios_finalize (rank);
    MPI_Finalize ();

    sprintf(result, "1D: rank=%d t=[%g", rank, t[0]);
    for (i=1; i<NX; i++) {
    	sprintf (s, ",%g", t[i]);
    	strcat (result, s);
    }
    printf("%s]\n", result);

    sprintf(result, "2D: rank=%d t_2d=[", rank);
    for (i=0; i<X; i++) {
    	sprintf (s, "[%d:%g", i, t_2d[i][0]);
    	strcat (result, s);

    	for (j=1; j < Y; j++){
    		sprintf (s, ",%g", t_2d[i][j]);
    		strcat (result, s);
    	}
    	sprintf (s, "],");
    	strcat (result,s);
    }
    printf("%s]\n", result);


    sprintf(result, "3D: rank=%d t_3d=[\n", rank);
    for (i=0; i<X; i++) {
        for (j=0; j < Y; j++){
        	for (k = 0; k < Y; k++){
        		sprintf (s, "\t[%d,%d,%d]=%g\n", i, j, k, t_3d[i][j][k]);
        		strcat (result, s);
        	}
        }
    }
    printf("%s]\n", result);

    return 0;
}

