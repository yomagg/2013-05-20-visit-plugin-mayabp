/**
 * bp_utils.h
 *
 *  Created on: Apr 19, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */

#ifndef BP_UTILS_H_
#define BP_UTILS_H_

extern int print_avi(ADIOS_FILE *fp, ADIOS_VARINFO *avi);
extern int print_stats(ADIOS_FILE *fp, ADIOS_VARINFO *avi);

extern int get_avi_type_size(ADIOS_VARINFO *avi);
extern int p_perform_reads_buffer(void * p, ADIOS_VARINFO *avi, uint64_t count);

#endif /* BP_UTILS_H_ */
